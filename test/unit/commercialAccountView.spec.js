import CommercialAccountView from '../../src/commercialAccountView';
import dummy from '../dummy';

/*
test methods
 */
describe('CommercialAccountView class', () => {
    describe('constructor', () => {
        it('throws if id is null', () => {
            /*
             arrange
             */
            const constructor = () =>
                new CommercialAccountView(
                    null,
                    dummy.address,
                    dummy.sapAccountNumber,
                    dummy.customerType,
                    dummy.customerSubType,
                    dummy.customerSubSubType,
                    dummy.customerBrand,
                    dummy.customerSubBrand,
                    dummy.phoneNumber
                );

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'id required');
        });
        it('sets id', () => {
            /*
             arrange
             */
            const expectedId = dummy.id;

            /*
             act
             */
            const objectUnderTest =
                new CommercialAccountView(
                    expectedId,
                    dummy.name,
                    dummy.address,
                    dummy.sapAccountNumber,
                    dummy.customerType,
                    dummy.customerSubType,
                    dummy.customerSubSubType,
                    dummy.customerBrand,
                    dummy.customerSubBrand,
                    dummy.phoneNumber
                );

            /*
             assert
             */
            const actualId = objectUnderTest.id;
            expect(actualId).toEqual(expectedId);

        });
        it('throws if name is null', () => {
            /*
             arrange
             */

            const constructor =
                () =>
                    new CommercialAccountView(
                        dummy.id,
                        null,
                        dummy.address,
                        dummy.sapAccountNumber,
                        dummy.customerType,
                        dummy.customerSubType,
                        dummy.customerSubSubType,
                        dummy.customerBrand,
                        dummy.customerSubBrand,
                        dummy.phoneNumber
                    );

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'name required');
        });
        it('sets name', () => {
            /*
             arrange
             */
            const expectedName = dummy.name;

            /*
             act
             */
            const objectUnderTest =
                new CommercialAccountView(
                    dummy.id,
                    expectedName,
                    dummy.address,
                    dummy.sapAccountNumber,
                    dummy.customerType,
                    dummy.customerSubType,
                    dummy.customerSubSubType,
                    dummy.customerBrand,
                    dummy.customerSubBrand,
                    dummy.phoneNumber
                );

            /*
             assert
             */
            const actualName = objectUnderTest.name;
            expect(actualName).toEqual(expectedName);

        });
        it('throws if address is null', () => {
            /*
             arrange
             */
            const constructor = () => new CommercialAccountView(
                dummy.id,
                dummy.name,
                null,
                dummy.sapAccountNumber,
                dummy.customerType,
                dummy.customerSubType,
                dummy.customerSubSubType,
                dummy.customerBrand,
                dummy.customerSubBrand,
                dummy.phoneNumber
            );

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'address required');
        });
        it('sets address', () => {
            /*
             arrange
             */
            const expectedAddress = dummy.address;

            /*
             act
             */
            const objectUnderTest =
                new CommercialAccountView(
                    dummy.id,
                    dummy.name,
                    expectedAddress,
                    dummy.sapAccountNumber,
                    dummy.customerType,
                    dummy.customerSubType,
                    dummy.customerSubSubType,
                    dummy.customerBrand,
                    dummy.customerSubBrand,
                    dummy.phoneNumber
                );

            /*
             assert
             */
            const actualAddress = objectUnderTest.address;
            expect(actualAddress).toEqual(expectedAddress);

        });
        it('throws if customerType is null', () => {
            /*
             arrange
             */
            const constructor = () => new CommercialAccountView(
                dummy.id,
                dummy.name,
                dummy.address,
                dummy.sapAccountNumber,
                null,
                dummy.customerSubType,
                dummy.customerSubSubType,
                dummy.customerBrand,
                dummy.customerSubBrand,
                dummy.phoneNumber
            );

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'customerType required');
        });
        it('sets customerType', () => {
            /*
             arrange
             */
            const expectedCustomerType = dummy.customerType;

            /*
             act
             */
            const objectUnderTest =
                new CommercialAccountView(
                    dummy.id,
                    dummy.name,
                    dummy.address,
                    dummy.sapAccountNumber,
                    expectedCustomerType,
                    dummy.customerSubType,
                    dummy.customerSubSubType,
                    dummy.customerBrand,
                    dummy.customerSubBrand,
                    dummy.phoneNumber
                );

            /*
             assert
             */
            const actualCustomerType = objectUnderTest.customerType;
            expect(actualCustomerType).toEqual(expectedCustomerType);

        });
        it('does not throws if customerBrand is null', () => {
            /*
             arrange
             */
            const constructor = () => new CommercialAccountView(
                dummy.id,
                dummy.name,
                dummy.address,
                dummy.sapAccountNumber,
                dummy.customerType,
                dummy.customerSubType,
                dummy.customerSubSubType,
                null,
                dummy.customerSubBrand,
                dummy.phoneNumber
            );

            /*
             act/assert
             */
            expect(constructor).not.toThrow();
        });
        it('sets customerBrand', () => {
            /*
             arrange
             */
            const expectedCustomerBrand = dummy.customerBrand;

            /*
             act
             */
            const objectUnderTest =
                new CommercialAccountView(
                    dummy.id,
                    dummy.name,
                    dummy.address,
                    dummy.sapAccountNumber,
                    dummy.customerType,
                    dummy.customerSubType,
                    dummy.customerSubSubType,
                    expectedCustomerBrand,
                    dummy.customerSubBrand,
                    dummy.phoneNumber
                );

            /*
             assert
             */
            const actualCustomerBrand = objectUnderTest.customerBrand;
            expect(actualCustomerBrand).toEqual(expectedCustomerBrand);

        });
        it('does not throw if phoneNumber is null',() =>{

            /*
             arrange
             */
            const constructor = () => new CommercialAccountView(
                dummy.id,
                dummy.name,
                dummy.address,
                dummy.sapAccountNumber,
                dummy.customerType,
                dummy.customerSubType,
                dummy.customerSubSubType,
                dummy.customerBrand,
                dummy.customerSubBrand,
                null
            );

            /*
             act/assert
             */
            expect(constructor).not.toThrow();
        });
        it('sets phoneNumber', () => {
            /*
             arrange
             */
            const expectedPhoneNumber = dummy.phoneNumber;

            /*
             act
             */
            const objectUnderTest =
                new CommercialAccountView(
                    dummy.id,
                    dummy.name,
                    dummy.address,
                    dummy.sapAccountNumber,
                    dummy.customerType,
                    dummy.customerSubType,
                    dummy.customerSubSubType,
                    dummy.customerBrand,
                    dummy.customerSubBrand,
                    expectedPhoneNumber
                );

            /*
             assert
             */
            const actualPhoneNumber = objectUnderTest.phoneNumber;
            expect(actualPhoneNumber).toEqual(expectedPhoneNumber);

        });

    })
});
