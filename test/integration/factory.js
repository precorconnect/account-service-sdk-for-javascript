import AddCommercialAccountReq from '../../src/addCommercialAccountReq';
import AddConsumerAccountReq from '../../src/addConsumerAccountReq';
import jwt from 'jwt-simple';
import config from './config';
import dummy from '../dummy';

export default {
    constructValidAddCommercialAccountRequest,
    constructValidAddConsumerAccountRequest,
    constructValidPartnerRepOAuth2AccessToken,
    constructValidAppAccessToken
}

function constructValidAddCommercialAccountRequest():AddCommercialAccountReq {

    return new AddCommercialAccountReq(
        dummy.name,
        dummy.address,
        dummy.phoneNumber,
        dummy.customerType,
        dummy.customerSubType,
        dummy.customerSubSubType,
        dummy.customerBrand,
        dummy.customerSubBrand
    )
}

function constructValidAddConsumerAccountRequest():AddConsumerAccountReq {

    return new AddConsumerAccountReq(
        dummy.firstName,
        dummy.lastName,
        dummy.address,
        dummy.phoneNumber,
        dummy.userId
    )
}

function constructValidAppAccessToken():string {

    const tenMinutesInMilliseconds = 10000 * 60;

    const jwtPayload = {
        "type": "app",
        "exp": Date.now() + tenMinutesInMilliseconds,
        "aud": dummy.url,
        "iss": dummy.url
    };

    return jwt.encode(jwtPayload, config.identityServiceJwtSigningKey);
}

function constructValidPartnerRepOAuth2AccessToken(accountId:string = dummy.accountId):string {

    const tenMinutesInMilliseconds = 10000 * 60;

    const jwtPayload = {
        "type": 'partnerRep',
        "exp": Date.now() + tenMinutesInMilliseconds,
        "aud": dummy.url,
        "iss": dummy.url,
        "given_name": dummy.firstName,
        "family_name": dummy.lastName,
        "sub": dummy.userId,
        "account_id": accountId,
        "sap_vendor_number": dummy.sap_vendor_number
    };

    return jwt.encode(jwtPayload, config.identityServiceJwtSigningKey);
}
