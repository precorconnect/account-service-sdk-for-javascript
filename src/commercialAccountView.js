import {PostalAddress} from 'postal-object-model';

/**
 * The most detailed view of a account
 * @class CommercialAccountView
 */
export default class CommercialAccountView {

    _id:string;

    _name:string;

    _address:PostalAddress;

    _sapAccountNumber:string;

    _customerType:string;

    _customerSubType:string;

    _customerSubSubType:string;

    _customerBrand:string;

    _customerSubBrand:string;

    _phoneNumber:string;



    /**
     * @param id
     * @param name
     * @param address
     * @param sapAccountNumber
     * @param customerType
     * @param customerSubType
     * @param customerSubSubType
     * @param customerBrand
     * @param customerSubBrand
     * @param phoneNumber
     */
    constructor(id:string,
                name:string,
                address:PostalAddress,
                sapAccountNumber:string,
                customerType:string,
                customerSubType:string,
                customerSubSubType:string,
                customerBrand:string,
                customerSubBrand:string,
                phoneNumber:string
    ){

        if (!id) {
            throw new TypeError('id required');
        }
        this._id = id;

        if (!name) {
            throw new TypeError('name required');
        }
        this._name = name;

        if (!address) {
            throw new TypeError('address required');
        }
        this._address = address;

        this._sapAccountNumber = sapAccountNumber;

        if(!customerType){
            throw new TypeError('customerType required');
        }
        this._customerType = customerType;

        this._customerSubType = customerSubType;

        this._customerSubSubType = customerSubSubType;

        this._customerBrand = customerBrand;

        this._customerSubBrand = customerSubBrand;

        this._phoneNumber = phoneNumber;

    }

    /**
     * @returns {string}
     */
    get id():string {
        return this._id;
    }

    /**
     * @returns {string}
     */
    get name():string {
        return this._name;
    }

    /**
     * @returns {PostalAddress}
     */
    get address():PostalAddress {
        return this._address;
    }

    get sapAccountNumber():string{
        return this._sapAccountNumber;
    }

    get customerType():string{
        return this._customerType;
    }

    get customerSubType():string{
        return this._customerSubType;
    }

    get customerSubSubType():string{
        return this._customerSubSubType;
    }

    get customerBrand():string{
        return this._customerBrand;
    }

    get customerSubBrand():string{
        return this._customerSubBrand;
    }

    get phoneNumber():string{
        return this._phoneNumber;
    }

    toJSON() {
        return {
            id: this._id,
            name: this._name,
            address: this._address,
            sapAccountNumber:this._sapAccountNumber,
            customerType:this._customerType,
            customerSubType:this._customerSubType,
            customerSubSubType:this._customerSubSubType,
            customerBrand:this._customerBrand,
            customerSubBrand:this._customerSubBrand,
            phoneNumber: this._phoneNumber
        }
    }

}
