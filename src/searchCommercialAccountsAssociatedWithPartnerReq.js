export default class SearchCommercialAccountsAssociatedWithPartnerReq {

    _partialName:string;

    _partnerAccountId:string;

    /**
     * @param {string} partialName
     * @param {string} partnerAccountId
     */
    constructor(partialName:string,
                partnerAccountId:string) {

        if (!partialName) {
            throw new TypeError('partialName required');
        }
        this._partialName = partialName;

        if (!partnerAccountId) {
            throw new TypeError('partnerAccountId required');
        }
        this._partnerAccountId = partnerAccountId;

    }

    /**
     * @returns {string}
     */
    get partialName():string {
        return this._partialName;
    }

    /**
     * @returns {string}
     */
    get partnerAccountId():string {
        return this._partnerAccountId;
    }

    toJSON() {
        return {
            partialName: this._partialName,
            partnerAccountId: this._partnerAccountId
        };
    }
}