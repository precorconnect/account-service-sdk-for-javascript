import {Container} from 'aurelia-dependency-injection';
import {HttpClient} from 'aurelia-http-client';
import AccountServiceSdkConfig from './accountServiceSdkConfig';
import AddCommercialAccountFeature from './addCommercialAccountFeature';
import GetCommercialAccountWithIdFeature from './getCommercialAccountWithIdFeature';
import SearchCommercialAccountsAssociatedWithPartnerFeature from './searchCommercialAccountsAssociatedWithPartnerFeature';
import SearchCommercialAccountsAssociatedWithPartnerAccountIdFeature from './searchCommercialAccountsAssociatedWithPartnerAccountIdFeature';
import AddConsumerAccountFeature from './addConsumerAccountFeature';

/**
 * @class {DiContainer}
 */
export default class DiContainer {

    _container:Container;

    /**
     * @param {AccountServiceSdkConfig} config
     */
    constructor(config:AccountServiceSdkConfig) {

        if (!config) {
            throw 'config required';
        }

        this._container = new Container();

        this._container.registerInstance(AccountServiceSdkConfig, config);
        this._container.autoRegister(HttpClient);

        this._registerFeatures();

    }

    /**
     * Resolves a single instance based on the provided key.
     * @param key The key that identifies the object to resolve.
     * @return Returns the resolved instance.
     */
    get(key:any):any {
        return this._container.get(key);
    }

    _registerFeatures() {

        this._container.autoRegister(AddCommercialAccountFeature);
        this._container.autoRegister(GetCommercialAccountWithIdFeature);
        this._container.autoRegister(SearchCommercialAccountsAssociatedWithPartnerFeature);
        this._container.autoRegister(SearchCommercialAccountsAssociatedWithPartnerAccountIdFeature);
        this._container.autoRegister(AddConsumerAccountFeature);
    }

}
