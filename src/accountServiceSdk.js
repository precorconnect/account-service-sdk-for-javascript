import AccountServiceSdkConfig from './accountServiceSdkConfig';
import DiContainer from './diContainer';
import AddCommercialAccountReq from './addCommercialAccountReq';
import AddCommercialAccountFeature from './addCommercialAccountFeature';
import CommercialAccountSynopsisView from './commercialAccountSynopsisView';
import CommercialAccountView from './commercialAccountView';
import GetCommercialAccountWithIdFeature from './getCommercialAccountWithIdFeature';
import SearchCommercialAccountsAssociatedWithPartnerReq from './searchCommercialAccountsAssociatedWithPartnerReq';
import SearchCommercialAccountsAssociatedWithPartnerFeature from './searchCommercialAccountsAssociatedWithPartnerFeature';
import SearchCommercialAccountsAssociatedWithPartnerAccountIdFeature from './searchCommercialAccountsAssociatedWithPartnerAccountIdFeature';
import AddConsumerAccountReq from './addConsumerAccountReq';
import AddConsumerAccountFeature from './addConsumerAccountFeature';
import ConsumerAccountId from './consumerAccountId';

/**
 * @class {AccountServiceSdk}
 */
export default class AccountServiceSdk {

    _diContainer:DiContainer;

    /**
     * @param {AccountServiceSdkConfig} config
     */
    constructor(config:AccountServiceSdkConfig) {

        this._diContainer = new DiContainer(config);

    }

    addCommercialAccount(request:AddCommercialAccountReq,
                         accessToken:string):Promise<string> {

        return this
            ._diContainer
            .get(AddCommercialAccountFeature)
            .execute(
                request,
                accessToken);

    }

    getCommercialAccountWithId(id:string,
                               accessToken:string):Promise<CommercialAccountView> {

        return this
            ._diContainer
            .get(GetCommercialAccountWithIdFeature)
            .execute(
                id,
                accessToken);
    }

    searchCommercialAccountsAssociatedWithPartner(request:SearchCommercialAccountsAssociatedWithPartnerReq,
                                                  accessToken:string):Promise<Array> {

        return this
            ._diContainer
            .get(SearchCommercialAccountsAssociatedWithPartnerFeature)
            .execute(
                request,
                accessToken);

    }

    searchCommercialAccountsAssociatedWithPartnerAccountId(partnerAccountId:string,
                                                           accessToken:string):Promise<Array> {

        return this
            ._diContainer
            .get(SearchCommercialAccountsAssociatedWithPartnerAccountIdFeature)
            .execute(
                partnerAccountId,
                accessToken);

    }

    addConsumerAccount(request:AddConsumerAccountReq,
                       accessToken:string):Promise<ConsumerAccountId> {

        return this
            ._diContainer
            .get(AddConsumerAccountFeature)
            .execute(
                request,
                accessToken);

    }
}




