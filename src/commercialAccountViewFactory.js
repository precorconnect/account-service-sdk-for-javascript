import CommercialAccountView from './commercialAccountView';
import {PostalAddressFactory} from 'postal-object-model';

export default class CommercialAccountViewFactory {

    static construct(data):CommercialAccountView {

        const name = data.name;

        const address = PostalAddressFactory.construct(data.address);

        const id = data.id;

        const sapAccountNumber = data.sapAccountNumber;

        const customerType = data.customerType;

        const customerSubType = data.customerSubType;

        const customerSubSubType = data.customerSubSubType;

        const customerBrand = data.customerBrand;

        const customerSubBrand = data.customerSubBrand;

        const phoneNumber = data.phoneNumber;

        return new CommercialAccountView(
            id,
            name,
            address,
            sapAccountNumber,
            customerType,
            customerSubType,
            customerSubSubType,
            customerBrand,
            customerSubBrand,
            phoneNumber
        );

    }

}
