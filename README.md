## Description
Precor Connect account service SDK for javascript.

## Features

##### Add Commercial Account
* [documentation](features/AddCommercialAccount.feature)

##### Get Commercial Account With Id
* [documentation](features/GetCommercialAccountWithId.feature)

##### Search Commercial Accounts Associated With Partner
* [documentation](features/SearchCommercialAccountsAssociatedWithPartner.feature)

## Setup

**install via jspm**  
```shell
jspm install account-service-sdk=bitbucket:precorconnect/account-service-sdk-for-javascript
```

**import & instantiate**
```javascript
import AccountServiceSdk,{AccountServiceSdkConfig} from 'account-service-sdk'

const accountServiceSdkConfig = 
    new AccountServiceSdkConfig(
        "https://api-dev.precorconnect.com"
    );
    
const accountServiceSdk = 
    new AccountServiceSdk(
        accountServiceSdkConfig
    );
```

## Platform Support

This library can be used in the **browser**.

## Develop

#### Software
- git
- npm

#### Scripts

install dependencies (perform prior to running or testing locally)
```PowerShell
npm install
```

unit & integration test in multiple browsers/platforms
```PowerShell
# note: following environment variables must be present:
# SAUCE_USERNAME
# SAUCE_ACCESS_KEY
npm test
```